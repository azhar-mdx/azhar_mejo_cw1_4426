# Authors
# 1. Azhar Asif
# 2. Mejo Thomas
# ------------------------

# References
# 1. Jaysinh Sagar | https://gitlab.com/jaysinh/opencv-tutorials
# 2. Mark Heywood | https://www.bluetin.io/opencv/object-detection-tracking-opencv-python/
# ------------------------

# Project: Object tracking
# ------------------------

# Importing necessary libraries
import cv2
import numpy as np

# Initialize video capture
video_capture = cv2.VideoCapture("data/1.mp4")

while True:
    # Get video frame
    status, frame = video_capture.read()

    # check if fram load status is true.
    if status:       
        # Convert the frame to HSV colour model.
        frameHSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        # HSV values to define a colour range we want to create a mask from.
        colorLow = np.array([0,0,0])
        colorHigh = np.array([35,255,40])

        # create a binary mask by keeping values that lie between high and low values
        objMask = cv2.inRange(frameHSV, colorLow, colorHigh)

        # Find Contours
        contours, hierarchy = cv2.findContours(objMask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # Finding The biggest contour to eliminate false positives and enabling real time tracking to track the nearest object
        contour_sizes = [(cv2.contourArea(contour), contour) for contour in contours]
        biggest_contour = max(contour_sizes, key=lambda x: x[0])[1]
        # Bounding Rectangle
        x,y,w,h = cv2.boundingRect(biggest_contour)
        cv2.rectangle(frame,(x,y),(x+w,y+h),(0,0,255),2)

        # Show final output
        cv2.imshow('output', frame)

        # Setting 'q' as an input to quit the video
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        # Setting 'q' as an input to quit the video
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        continue    

# release the video capture object once we are done and destroy all windows    
cv2.destroyAllWindows()
video_capture.release()