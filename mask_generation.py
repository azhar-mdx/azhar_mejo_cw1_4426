# Importing necessary libraries
import cv2
import numpy as np

# Initialize video capture
video_capture = cv2.VideoCapture("data/1.mp4")

while True:
    # Get video frame
    status, frame = video_capture.read()

    # check if fram load status is true.
    if status:       
        # Convert the frame to HSV colour model.
        frameHSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        # HSV values to define a colour range we want to create a mask from.
        colorLow = np.array([0,0,0])
        colorHigh = np.array([35,255,40])

        # create a binary mask by keeping values that lie between high and low values
        objMask = cv2.inRange(frameHSV, colorLow, colorHigh)

        # Show the mask
        cv2.imshow('objectMask', objMask)

        # Setting 'q' as an input to quit the video
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        # Setting 'q' as an input to quit the video
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        continue    

# release the video capture object once we are done and destroy all windows    
cv2.destroyAllWindows()
video_capture.release()