# import libraries
import cv2
import numpy as np

# create a video capture object and load the video
video_capture = cv2.VideoCapture("data/1.mp4")

while True:

    status, frame = video_capture.read()

    # check if fram load status is true. Can also be written
    # as if status == True
    if status:
        # create a window called Video and show the frame
        edged_frame = cv2.Canny(frame, 110, 280)
        cv2.imshow('Edges', edged_frame)
        # wait for 1ms per frame to see of there is any input
        # here we have set 'q' as an input to quit the video
        # stream
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        continue

# release the video capture object once we are done and destroy
# all windows
video_capture.release()
cv2.destroyAllWindows()